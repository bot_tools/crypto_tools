defmodule Binance.Api.Config do
  defstruct [
    :api_key,
    :api_secret,
    :api_endpoint
  ]
end
