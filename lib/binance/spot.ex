defmodule Binance.Spot do
  alias Binance.Api
  alias Decimal, as: D

  @allCoins "/sapi/v1/capital/config/getall"
  @exchangeInfo "/api/v3/exchangeInfo"
  @exchangeInfo "/api/v3/exchangeInfo"
  @getAccount "/api/v3/account"
  @getAllOrders "/api/v3/allOrders"
  @getMyTrades "/api/v3/myTrades"
  @getOpenOrders "/api/v3/openOrders"
  @getOrderList "/api/v3/orderList"
  @getOrder "/api/v3/order"
  @getTicker "/api/v3/ticker/24hr"
  @newOrder "/api/v3/order"
  @newTestOrder "/api/v3/order/test"
  @systemStatus "/sapi/v1/system/status"
  @getPrice "/api/v3/ticker/price"
  @newOCOOrder "/api/v3/order/oco"



  def delete_order(params, config \\ nil),            do: Api.delete(@newOrder, params, get_config(config))
  def get_all_coins(config \\ nil),                   do: Api.get(@allCoins, %{}, get_config(config))
  def get_all_orders(params, config \\ nil),          do: Api.get(@getAllOrders, params, get_config(config))
  def get_exchenge_info(config \\ nil),               do: Api.get_unsigned(@exchangeInfo, %{}, get_config(config))
  def get_my_account(config \\ nil),                  do: Api.get(@getAccount, %{}, get_config(config))
  def get_my_trades(params, config \\ nil),           do: Api.get(@getMyTrades, params, get_config(config))
  def get_open_orders(params \\ %{}, config \\ nil),  do: Api.get(@getOpenOrders, params, get_config(config))
  def get_order(params \\ %{}, config \\ nil),        do: Api.get(@getOrder, params, get_config(config))
  def get_order_list(params \\ %{}, config \\ nil),   do: Api.get(@getOrderList, params, get_config(config))
  def get_price(params, config \\ nil),               do: Api.get_unsigned(@getPrice, params, get_config(config))
  def get_ticker(params, config \\ nil),              do: Api.get_unsigned(@getTicker, params, get_config(config))
  def new_oco_order(params, config \\ nil),           do: Api.post(@newOCOOrder, params, get_config(config))
  def new_order(params, config \\ nil),               do: Api.post(@newOrder, params, get_config(config))
  def new_test_order(params, config \\ nil),          do: Api.post(@newTestOrder, params, get_config(config))
  def get_kline_data(params, config \\ nil),          do: Api.get_public("/api/v3/klines", params, get_config(config))

  defp get_config(%Binance.Api.Config{} = config), do: config
  defp get_config(nil) do
    %Binance.Api.Config{
      api_key: Application.get_env(:crypto_tools, :binance_api_key),
      api_secret: Application.get_env(:crypto_tools, :binance_api_secret_key),
      api_endpoint: Application.get_env(:crypto_tools, :binance_api_endpoint),
    }
  end

  def create_config(key, secret) do
    %Binance.Api.Config{
      api_key: key,
      api_secret: secret,
      api_endpoint: Application.get_env(:crypto_tools, :binance_api_endpoint)
    }
  end


end
