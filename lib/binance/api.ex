defmodule Binance.Api do

  def post(route, params, config \\ nil) do
    {api_endpoint, api_key, api_secret} = read_config(config)
    query = to_query_string(params)
    sig = sign(query, api_secret)
    HTTPoison.post(api_endpoint <> route <> "?signature=#{sig}", query, headers(api_key))
    |> normalize_response()
  end

  def delete(route, params, config \\ nil) do
    {api_endpoint, api_key, api_secret} = read_config(config)
    query = to_query_string(params)
    sig = sign(query, api_secret)
    HTTPoison.delete(api_endpoint <> route <> "?#{query}&signature=#{sig}", headers(api_key))
    |> normalize_response()
  end

  def get(route, params \\ %{}, config \\ nil) do
    {api_endpoint, api_key, api_secret} = read_config(config)
    query = to_query_string(params)
    sig = sign(query, api_secret)
    HTTPoison.get(api_endpoint <> route <> "?#{query}&signature=#{sig}", headers(api_key))
    |> normalize_response()
  end

  def get_public(route, params \\ %{}, config) do
    {api_endpoint, _, _} = read_config(config)
    query =
      params
      |> Enum.map(fn ({k,v}) -> "#{k}=#{v}" end)
      |> Enum.join("&")
    HTTPoison.get(api_endpoint <> route <> "?#{query}", [])
    |> normalize_response()
  end

  def get_unsigned(route, params \\ %{}, config \\ nil) do
    {api_endpoint, api_key, _api_secret} = read_config(config)
    query = params |> Enum.map(fn ({k,v}) -> "#{k}=#{v}" end) |> Enum.join("&")
    HTTPoison.get(api_endpoint <> route <> "?#{query}", headers(api_key))
    |> normalize_response()
  end

  defp headers(api_key) do
    [{"X-MBX-APIKEY", api_key}, {"content-type", "application/x-www-form-urlencoded"}]
  end

  defp sign(query, api_secret) do
    :crypto.mac(:hmac, :sha256, api_secret, query) |> Base.encode16(case: :lower)
  end

  defp read_config(%Binance.Api.Config{} = c) do
    {c.api_endpoint, c.api_key, c.api_secret}
  end

  defp to_query_string(params) when is_map(params) do
    params
    |> Map.merge(%{"timestamp" => :os.system_time(:millisecond)})
    |> Enum.map(fn ({k,v}) -> "#{k}=#{v}" end)
    |> Enum.join("&")
  end

  defp normalize_response(resp) do
    case resp do
      {:ok, %{body: body}} -> Jason.decode!(body)
      error -> error
    end
  end

end
