defmodule Binance.Futures do
  alias Binance.Api

  def create_config(key, secret) do
    %Binance.Api.Config{
      api_key: key,
      api_secret: secret,
      api_endpoint: Application.get_env(:crypto_tools, :binance_futures_api_endpoint)
    }
  end

  defp get_config(%Binance.Api.Config{} = config), do: config
  defp get_config(nil) do
    %Binance.Api.Config{
      api_key: Application.get_env(:crypto_tools, :binance_futures_api_key),
      api_secret: Application.get_env(:crypto_tools, :binance_futures_api_secret_key),
      api_endpoint: Application.get_env(:crypto_tools, :binance_futures_api_endpoint),
    }
  end


  def exchange_information() do
    Api.get_public("/fapi/v1/exchangeInfo", %{}, get_config(nil))
  end

  def get_current_multi_assets_mode(config \\ nil) do
    Api.get("/fapi/v1/multiAssetsMargin", %{}, get_config(config))
  end

  def change_multi_asset_mode(mode, config \\ nil) when is_boolean(mode) do
    Api.post("/fapi/v1/multiAssetsMargin", %{multiAssetsMargin: mode}, get_config(config))
  end

  def change_initial_leverage(params, config \\ nil) do
    Api.post("/fapi/v1/leverage", params, get_config(config))
  end

  def change_margin_type(params, config \\ nil) do
    Api.post("/fapi/v1/marginType", params, get_config(config))
  end

  def account_information(config \\ nil) do
    Api.get("/fapi/v2/account", %{}, get_config(config))
  end

  def get_mark_price(params \\ %{}, config \\ nil) do
    Api.get_public("/fapi/v1/premiumIndex", params, get_config(config))
  end

  def get_income_history(params \\ %{}, config \\ nil) do
    Api.get("/fapi/v1/income", params, get_config(config))
  end

  def new_order(params, config \\ nil) do
    Api.post("/fapi/v1/order", params, get_config(config))
  end

  def cancel_all_open_orders(params, config \\ nil) do
    Api.delete("/fapi/v1/allOpenOrders", params, get_config(config))
  end

end

