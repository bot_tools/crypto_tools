defmodule ByBit.Websocket do
  use WebSockex
  @pong_interval 5*60*1000

  def start_link(state) do
    {:ok, pid} = WebSockex.start_link("wss://stream-testnet.bybit.com/realtime_private", __MODULE__, state, debug: [:trace])
    |> IO.inspect
    #Process.send_after(pid, :send_pong, @pong_interval)
    Process.send_after(pid, :init_private_channel, 100)
    {:ok, pid}
  end


  def handle_frame({type, msg}, state) do
    IO.inspect type
    case Jason.decode!(msg) do
      %{"request" => %{"op" => "auth"}, "success" => true} ->
        IO.inspect "AUTH OK"
        json = Jason.encode!(%{op: "subscribe", args: ["order", "position"]})
        {:reply, {:text, json}, state}
      other ->
        IO.inspect(other)
        {:ok, state}
    end
  end

  def handle_info(:init_channel, state) do
    json = Jason.encode!(%{op: "subscribe", args: ["orderbook.25.BTCUSDT"]})
    {:reply, {:text, json}, state}
  end

  def handle_info(:init_private_channel, state) do
    timestamp = DateTime.utc_now() |> DateTime.to_unix(:millisecond)
    expires = timestamp + 10000
    signature = sign("GET/realtime#{expires}", nil)
    payload = %{
      op: "auth",
      args: [api_key(), expires, signature]
    } |> Jason.encode!

    #json = Jason.encode!(%{op: "subscribe", args: ["order"]})
    #Process.send_after(self(), :send_ping, 1000)
    {:reply, {:text, payload}, state}
  end


  def handle_info(:send_ping, state) do
    Process.send_after(self(), :send_ping, 30000)
    {:reply, {:text, Jason.encode!(%{"op" => "ping"}), state}}
  end

  defp handle_auth_success(state) do
    json = Jason.encode!(%{op: "subscribe", args: ["position"]})
    {:reply, {:text, json}, state}
  end

  def api_key(), do: "zrbor9FuXZ1tfZ0V6m"
  def api_secret(), do: "nKGRp35Xq69VtTTnyJuRBvlYBecWO9vGWeYT"
  def api_host(), do: "https://api-testnet.bybit.com"

  def sign(data, _secret) do
    :crypto.mac(:hmac, :sha256, api_secret(), data) |> Base.encode16(case: :lower)
  end
end
