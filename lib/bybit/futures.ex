defmodule ByBit.Futures do
  alias ByBit.Api

  def create_config(key, secret) do
    %ByBit.Api.Config{
      api_key: key,
      api_secret: secret,
      api_endpoint: Application.get_env(:crypto_tools, :bybit_futures_api_endpoint)
    }
  end

  def create_config(key, secret, endpont) do
    create_config(key, secret)
    |> Map.put(:api_endpoint, endpont)
  end

  def place_active_order(params, exchange_config \\ nil) do
    Api.post("/private/linear/order/create", params, exchange_config)
  end

  def get_active_order(params, exchange_config \\ nil) do
    Api.get("/private/linear/order/list", params, exchange_config)
  end

  def cancel_active_order(params, exchange_config \\ nil) do
    Api.post("/private/linear/order/cancel", params, exchange_config)
  end

  def my_position_list(params, exchange_config \\ nil) do
    Api.get("/private/linear/position/list", params, exchange_config)
  end

  def set_leverage(params, exchange_config \\ nil) do
    Api.post("/private/linear/position/set-leverage", params, exchange_config)
  end

  def position_mode_switch(params, exchange_config \\ nil) do
    Api.post("/contract/v3/private/position/switch-mode", params, exchange_config)
  end

  def get_wallet_balance(params, exchange_config \\ nil) do
    Api.get("/v2/private/wallet/balance", params, exchange_config)
  end

  def get_instruments_info(params, exchange_config \\ nil) do
    Api.get("/derivatives/v3/public/instruments-info", params, exchange_config)
  end
end
