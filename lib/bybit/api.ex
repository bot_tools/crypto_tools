defmodule ByBit.Api do

  def get(route, params, config \\ nil) do
    {api_endpoint, api_key, api_secret} = read_config(config)
    request_params = request_params(params, api_key)
    param_str = params_to_query(request_params, api_key)
    sign = sign(param_str, api_secret)

    {:ok, %{body: body}} =
      HTTPoison.get("#{api_endpoint}#{route}?#{param_str}&sign=#{sign}")
    Jason.decode!(body)
  end

  def post(route, params, config \\ nil) do
    {api_endpoint, api_key, api_secret} = read_config(config)
    request_params = request_params(params, api_key)
    param_str = params_to_query(request_params, api_key)
    sign = sign(param_str, api_secret)
    final_params = Map.put(request_params, "sign", sign)
    headers = [{"Content-Type", "application/json"}]

    {:ok, %{body: body}} =
      HTTPoison.post("#{api_endpoint}#{route}", Jason.encode!(final_params), headers)
    Jason.decode!(body)
  end

  defp request_params(params, api_key) do
    timestamp = DateTime.utc_now() |> DateTime.to_unix(:millisecond)
    params
    |> Enum.map(fn
      {k, v} when is_atom(k) -> {Atom.to_string(k), v}
      {k, v} -> {k, v}
    end)
    |> Enum.into(%{})
    |> Map.merge(%{
      "api_key" => api_key,
      "timestamp" => timestamp
    })
  end

  defp params_to_query(params, api_key) do
    params
    |> Map.to_list
    |> Enum.sort_by(fn {k, _} -> k end)
    |> Enum.reverse()
    |> Enum.reduce([], fn {k, v}, acc -> ["#{k}=#{v}" | acc] end)
    |> Enum.join("&")
  end

  defp read_config(%ByBit.Api.Config{} = c) do
    {c.api_endpoint, c.api_key, c.api_secret}
  end

  defp read_config(nil) do
    {api_host(), api_key(), api_secret()}
  end

  def api_key(), do: Application.get_env(:crypto_tools, :bybit_futures_api_key)
  def api_secret(), do: Application.get_env(:crypto_tools, :bybit_futures_api_secret_key)
  def api_host(), do: Application.get_env(:crypto_tools, :bybit_futures_api_endpoint)


  def test do
    timestamp = DateTime.utc_now() |> DateTime.to_unix(:millisecond)
                |> IO.inspect
    param_str = "api_key=#{api_key()}&symbol=BTCUSDT&timestamp=#{timestamp}"
                |> IO.inspect

    sign = sign(param_str, nil)
           |> IO.inspect
    endpont = "/private/linear/position/list"
    {:ok, %{body: body}} = HTTPoison.get("https://api-testnet.bybit.com#{endpont}?#{param_str}&sign=#{sign}")
    Jason.decode(body)

  end

  def sign(data, secret) do
    :crypto.mac(:hmac, :sha256, secret, data) |> Base.encode16(case: :lower)
  end

  def test_ws do

  end
end
