defmodule FTX.Api do

  def api_key(),      do: Application.get_env(:crypto_tools, :ftx_futures_api_key)
  def api_secret(),   do: Application.get_env(:crypto_tools, :ftx_futures_api_secret_key)
  def api_endpoint(), do: Application.get_env(:crypto_tools, :ftx_futures_api_endpoint)
  def subaccount(),   do: Application.get_env(:crypto_tools, :ftx_futures_subaccount)


  def post(route, params \\ %{}, config \\ nil) do
    config = {api_endpoint, api_key, api_secret,_} = read_config(config)
    body = Jason.encode!(params)
    HTTPoison.post(api_endpoint <> route, body, headers(config, "POST", route, body))
    |> normalize_response()
  end

  def delete(route, params \\ %{}, config \\ nil) do
    config = {api_endpoint, api_key, api_secret,_} = read_config(config)
    body = Jason.encode!(params)
    HTTPoison.request(:delete, api_endpoint <> route, body, headers(config, "DELETE", route, body))
    |> normalize_response()
  end

  def get(route, params \\ %{}, config \\ nil) do
    config = {api_endpoint, api_key, api_secret, _} = read_config(config)
    HTTPoison.get(api_endpoint <> route, headers(config, "GET", route))
    |> normalize_response()
  end

  def get_public(route, params \\ %{}) do
    {api_endpoint, api_key, api_secret,_} = read_config(nil)
    HTTPoison.get(api_endpoint <> route, [{"content-type", "application/json"}])
    |> normalize_response()
  end

  defp headers({_, api_key, api_secret, subaccount}, method, path \\ "", body \\"") do
    ts = :os.system_time(:millisecond) |> to_string
    sign = ts<>method<>"/api"<>path<>body
    signed = sign(sign, api_secret)
    [
      {"FTX-KEY", api_key},
      {"FTX-TS", ts},
      {"FTX-SIGN", signed},
      {"content-type", "application/json"}
    ]
    |> maybe_put_subaccount(subaccount)
  end
  defp maybe_put_subaccount(kl, nil), do: kl
  defp maybe_put_subaccount(kl, s), do: [{"FTX-SUBACCOUNT", s} | kl]

  defp sign(query, api_secret) do
    :crypto.mac(:hmac, :sha256, api_secret, query) |> Base.encode16(case: :lower)
  end

  defp read_config(nil) do
    {api_endpoint(), api_key(), api_secret(), subaccount()}
  end
  defp read_config(%FTX.Api.Config{} = c) do
    {c.api_endpoint, c.api_key, c.api_secret, c.subaccount}
  end

  defp normalize_response(resp) do
    case resp do
      {:ok, %{body: body}} -> Jason.decode!(body)
      error -> error
    end
  end

end
