defmodule FTX.Api.Config do
  defstruct [
    :api_key,
    :api_secret,
    :api_endpoint,
    :subaccount
  ]
end
