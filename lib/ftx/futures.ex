defmodule FTX.Futures do
  alias FTX.Api

  def create_config(key, secret, subaccount \\ nil) do
    %FTX.Api.Config{
      api_key: key,
      api_secret: secret,
      api_endpoint: Application.get_env(:crypto_tools, :ftx_futures_api_endpoint),
      subaccount: subaccount
    }
  end

  def get_account_information(config \\ nil) do
    Api.get("/account", %{}, config)
    |> parse_result()
  end

  def get_positions(config \\ nil) do
    Api.get("/positions?showAvgPrice=true", %{}, config)
    |> parse_result()
  end

  def change_account_leverage(leverage, config \\ nil) do
    Api.post("/account/leverage", %{leverage: leverage}, config)
    |> parse_result()
  end

  def get_markets() do
    Api.get_public("/markets")
  end

  def get_orders(config \\ nil) do
    Api.get("/orders?market=ATOM-PERP", config)
  end
  def get_orders_history(params \\ %{}, config \\nil) do
    Api.get("/orders/history", params, config)
  end

  def place_order(params, config \\ nil) do
    Api.post("/orders", params, config)
  end

  def cancell_all_orders(params \\ %{}, config \\ nil) do
    Api.delete("/orders", params, config)
  end

  defp parse_result(%{"success" => true, "result" => result}) do
    result
  end
  defp parse_result(error_result), do: error_result
end
