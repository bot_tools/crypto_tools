import Config

config :crypto_tools,
  binance_api_key: System.get_env("BINANCE_API_KEY"),
  binance_api_secret_key: System.get_env("BINANCE_SECRET_KEY"),
  binance_api_endpoint: System.get_env("BINANCE_API_ENDPOINT"),
  binance_futures_api_key: System.get_env("BINANCE_FUTURES_API_KEY"),
  binance_futures_api_secret_key: System.get_env("BINANCE_FUTURES_API_SECRET_KEY"),
  binance_futures_api_endpoint: System.get_env("BINANCE_FUTURES_API_ENDPOINT"),
  ftx_futures_api_key: System.get_env("FTX_FUTURES_API_KEY"),
  ftx_futures_api_secret_key: System.get_env("FTX_FUTURES_API_SECRET_KEY"),
  ftx_futures_api_endpoint: System.get_env("FTX_FUTURES_API_ENDPOINT"),
  ftx_futures_subaccount: System.get_env("FTX_FUTURES_SUBACCOUNT"),
  bybit_futures_api_endpoint: System.get_env("BYBIT_FUTURES_API_ENDPOINT"),
  bybit_futures_api_key: System.get_env("BYBIT_FUTURES_API_KEY"),
  bybit_futures_api_secret_key: System.get_env("BYBIT_FUTURES_API_SECRET_KEY")
